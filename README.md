# Orange Cat 小橘

## 介绍
`Orange Cat` 是一个基于 `NXOS` 内核的游戏机操作系统，可以应用于游戏掌机和游戏主机。“由于整个这个项目是一只小橘猫开发的，所以命名为小橘。”

预计支持 `FC` 游戏，街机游戏，以及其它掌机游戏。

## 小橘

“喵生本无意义，只是来走这一遭罢了。” -- 小橘

![小橘图片](doc/figures/小橘.jpg)

## 使用方法

准备子模块：

```shell
# 更新子模块以及NXOS子模块
make prepare
```

配置内核：

```shell
# 1. 进入内核
cd nxos 
# 2. 设置环境
setup.bat
source setup.sh
# 3. 配置并保存内核
make defconfig # 获取默认配置
make menuconfig # 进行配置，参考下面的配置选项
make saveconfig # 保存当前配置
# 4. 离开内核
cd ..
```

配置内核勾选选项：

```
-> Platform:
    [*] probe veb driver
    [*] enable framebuffer driver
    [*] enable ps/2 mouse driver
    [*] enable ps/2 keyboard driver
```

编译运行：

```shell
# 1. 编译项目
make build
# 2. 运行项目
make run
```
